#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "../../task1/497Glushenko/common/LightInfo.hpp"
#include "../../task1/497Glushenko/common/Texture.hpp"

#include <iostream>
#include <vector>
#include <fstream>

class SampleApplication : public Application
{
public:
    std::vector<std::vector<int>> lab;
    MeshPtr _labyrinth;
    std::shared_ptr<CameraMover> _freeCam    = std::make_shared<FreeCameraMover>();
    std::shared_ptr<NotFreeCameraMover> _notFreeCam = std::make_shared<NotFreeCameraMover>(lab);

    MeshPtr _marker; //Маркер для источника света



    SampleApplication(std::vector<std::vector<int>> lab): lab(lab){};

    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;

    //Переменные для управления положением одного источника света
    float _lr = 3.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    LightInfo _light;

    TexturePtr _worldTexture;

    GLuint _sampler;

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = _freeCam;

        //Создаем меш с кубом
        _labyrinth = makeLabyrinth(lab);
        //_labyrinth->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        //Создаем шейдерную программу
        _shader = std::make_shared<ShaderProgram>("497GlushenkoData/shaderNormal.vert", "497GlushenkoData/shader.frag");

        //Инициализация шейдеров

        //_shader = std::make_shared<ShaderProgram>("497GlushenkoData/texture.vert", "shaders4/texture.frag");
        _markerShader = std::make_shared<ShaderProgram>("497GlushenkoData/marker.vert", "497GlushenkoData/marker.frag");

        //Инициализация значений переменных освщения
        _light.position = glm::vec3(0.0f, 0.0f, 0.5f);
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);
    }

    /*void update() override
    {
        Application::update();

        //Вращаем кролика
        float angle = static_cast<float>(glfwGetTime());

        glm::mat4 mat;
        mat = glm::translate(mat, glm::vec3(0.0f, 0.5f, 0.0));
        mat = glm::rotate(mat, angle, glm::vec3(0.0f, 0.0f, 1.0f));

        _bunny->setModelMatrix(mat);
    }*/

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);


        _light.position = _notFreeCam->getPos();
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        GLuint textureUnitForDiffuseTex = 0;

        if (USE_DSA) {
            glBindTextureUnit(textureUnitForDiffuseTex, _worldTexture->texture());
            glBindSampler(textureUnitForDiffuseTex, _sampler);
        }
        else {
            glBindSampler(textureUnitForDiffuseTex, _sampler);
            glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex);  //текстурный юнит 0
            _worldTexture->bind();
        }
        _shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex);

        //Рисуем первый меш
        _shader->setMat4Uniform("modelMatrix", _labyrinth->modelMatrix());
        _labyrinth->draw();

        //Рисуем второй меш
        //_shader->setMat4Uniform("modelMatrix", _bunny->modelMatrix());
        //_bunny->draw();

        //Рисуем маркеры для всех источников света
        {
            _markerShader->use();

            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker->draw();
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);

        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_1)
            {
                _cameraMover = _freeCam;
            } else if (key == GLFW_KEY_2) {
                _cameraMover = _notFreeCam;
            }
        }
    }
};

int main()
{
    std::vector<std::vector<int>> labyrinth;
    std::ifstream myfile("/home/anatoly/CompGraphics/tasks/task1/497Glushenko/labyrinth.txt");
    int length, width;
    int k;
    myfile >> width >> length;
    labyrinth.resize(width);
    for(int i = 0; i < width; ++i){
        for(int j = 0; j < length; j++){
            myfile >> k;
            labyrinth[i].push_back(k);
        }
    }

    SampleApplication app(labyrinth);
    std::cout << width << ' ' << length << std::endl;
    app.start();

    return 0;
}